//De app.js is het entrypoint van je api
const express = require('express')
const appartmentRoutes = require('./src/routes/appartment.routes')
const AuthenticationRoutes = require('./src/routes/authentication.routes')
const logger = require('./src/config/dbconfig').logger

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())

// Generic endpoint handler - voor alle routes

app.all('*', (req, res, next) => {

  // logger.info('Generieke afhandeling aangeroepen!')

  const { method, url } = req
  logger.info(`${method} ${url}`)
  next()
})

// Hier installeren we de routes
app.use('/api/', AuthenticationRoutes)
app.use('/api/appartments', appartmentRoutes)

// Handle endpoint not found.
app.all('*', (req, res, next) => {
  const { method, url } = req
  const errorMessage = `${method} ${url} does not exist.`
  logger.warn(errorMessage)
  const errorObject = {
    message: errorMessage,
    code: 404,
    date: new Date()
  }
  next(errorObject)
})

// Error handler
app.use((error, req, res, next) => {
  logger.error('Error handler: ', error.message.toString())
  res.status(error.code).json(error)
})

app.listen(port, () => logger.info(`App listening on port: ${port}!`))

module.exports = app
