// Dit javascript bestand koppelt de controller en de functies aan elkaar.
const express = require('express')
const router = express.Router()
const AppartmentController = require('../controllers/appartment.controller')
const AuthenticationController = require('../controllers/authentication.controller')

// Authentication routes
router.get('/', AppartmentController.getAllApartments) 
router.post('/', AuthenticationController.validateToken, AppartmentController.createApartment) 
router.get('/:apartmentId', AppartmentController.getApartmentById) 
router.put('/:apartmentId', AuthenticationController.validateToken, AppartmentController.updateApartment) 
router.delete('/:apartmentId', AuthenticationController.validateToken, AppartmentController.deleteApartmentById) 

router.post('/:apartmentId/reservations', AuthenticationController.validateToken, AppartmentController.createReservation) 
router.get('/:apartmentId/reservations', AppartmentController.getAllReservations) 
router.get('/:apartmentId/reservations/:reservationId', AppartmentController.getSpecificReservations) 
router.put('/:apartmentId/reservations/:reservationId', AuthenticationController.validateToken, AppartmentController.updateReservation) 
router.delete('/:apartmentId/reservations/:reservationId', AuthenticationController.validateToken, AppartmentController.deleteReservationById) 

module.exports = router
