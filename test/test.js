//In dit javascript bestand worden de tests uitgevoerd, er zijn in totaal 10 tests
const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')
const server = require('../app')

chai.should()
chai.use(chaiHttp)

const endpointapartments = '/api/appartments'
const endpointreservaton = '/api/appartments/1232/reservations'
const authorizationHeader = 'Authorization'
let token
var ApartmentToDeleteId

//
// Deze functie wordt voorafgaand aan alle tests 1 x uitgevoerd.
//
before(() => {
  console.log('- before: get valid token')
  // We hebben een token nodig om een ingelogde gebruiker te simuleren.
  // Hier kiezen we ervoor om een token voor UserId 1 te gebruiken, dit is de eerste gebruiker in de database.
  const payload = {
    UserId: 1
  }
  jwt.sign({ data: payload }, 'secretkey', { expiresIn: 2 * 60 }, (err, result) => {
    if (result) {
      token = result
    }
  })
})

//1. (GET) Geef een lijst met alle appartementen, met de gegevens van de verhuurder en alle reserveringen op dat apartement  
describe('Appartments API GET', () => {
  it('should return an array of Apartments', done => {
    chai
      .request(server)
      .get(endpointapartments)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      const apartment = result[0]

      apartment.should.have.property('UserId')
      apartment.should.have.property('status')
      apartment.should.have.property('StreetAddress')
      apartment.should.have.property('Description')
      apartment.should.have.property('PostalCode')
      apartment.should.not.have.property('Password')
      done()
     })
    })
})

//2. (POST) Voeg nieuw appartement toe
describe('Appartments API POST', () => {
  it('should return a valid id when posting a valid object', done => {
    chai
      .request(server)
      .post(endpointapartments)
      .set(authorizationHeader, token)
      .send({
        description: 'test description',
        streetaddress: 'test address',
        postalcode: '4681CP',
        city: 'testing'
      })
      .end((err, res) => {
        res.should.have.status(201)
        res.body.should.be.a('object')

        const result = res.body.result
        const apartment = result[0]

        apartment.should.have.property('StreetAddress')
        apartment.should.have.property('Description')
        apartment.should.have.property('PostalCode')
        apartment.should.not.have.property('Password')
        apartment.should.have.property('ApartmentId')
        done()
      })
  })
})

//3. (GET) Geef het gevraagde apartement, met de gegevens van de verhuurder en alle reserveringen op dat appartement.
describe('Appartments API GET', () => {
  it('should return the Appartment that corresponds with the given ID', done => {
    chai
      .request(server)
      .get(endpointapartments + '/' + 1232)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      const apartment = result[0]

      apartment.should.have.property('StreetAddress')
      apartment.should.have.property('Description')
      apartment.should.have.property('PostalCode')
      apartment.should.not.have.property('Password')
      done()
     })
    })
})

//4. (PUT) Update apartement met de gegeven id. Alleen door de eigenaar
describe('Apartment API PUT', () => {
  it('should change the information of an apartment', done => {
    chai
      .request(server)
      .put(endpointapartments + '/' + 1232)
      .set(authorizationHeader, token)
      .send({
        description: 'Avans Hogeschool',
        streetaddress: 'Nieuw Eijycklaan 1',
        postalcode: '3388AC',
        city: 'Breda'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        const result = res.body.result
        console.log(result)
      
        const apartment = result
        apartment.should.have.property('streetaddress')
        apartment.should.have.property('description')
        apartment.should.have.property('postalcode')
        apartment.should.not.have.property('Password')
        done()
      })
  })
})
//5. (DELETE) Verwijder appartement met de gegeven id. Alleen door eigenaar.

//We moeten eerst een Apartement posten, voordat we het kunnen verwijderen
describe('Apartment API DELETE', () => {
  before(function(done){
    this.enableTimeouts(false) 
    chai
    .request(server)
    .post(endpointapartments)
    .send({
      description: 'testdescription',
      streetaddress: 'testaddress',
      postalcode: '1000CP',
      city: 'testing'
    })
    .set(authorizationHeader, token)
    .end(function(err, res) {
      const result = res.body.result
      const apartment = result[0]
      ApartmentToDeleteId = apartment.ApartmentId
      done()
    })
  }

  )
  //Hier wordt het Apartement daadwerkelijk verwijderd.
it('should be able to delete Apartment if the user is the owner of the given Apartment', done => {
  chai
  .request(server)
  .delete(endpointapartments + '/' + ApartmentToDeleteId)
  .set(authorizationHeader, token)
  .send()
  .end((err, res) => {
    res.should.have.status(200)
    done()

  })
})
})
  

//6. (POST) Nieuwe reservering op dit apartement
describe('Reservation API POST', () => {
  it('A new reservation should be added and the test should return the correct code', done => {
    chai
      .request(server)
      .post(endpointreservaton)
      .set(authorizationHeader, token)
      .send({
        status: 'Initial',
        startdate: '2015-2-2',
        enddate: '2019-2-1'
      })
    .end((err, res) => {
      res.should.have.status(201)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      done()
     })
    })
})
//7. (GET) Alle reserveringen voor dit apartement
describe('Reservations API GET', () => {
  it('should return an array of Reservations', done => {
    chai
      .request(server)
      .get(endpointreservaton)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      result.should.be.an('array')
      const apartment = result[0]

      apartment.should.have.property('ReservationId')
      apartment.should.have.property('ApartmentId')
      apartment.should.have.property('StartDate')
      apartment.should.have.property('EndDate')
      apartment.should.have.property('UserId')
      apartment.should.have.property('Status')
      
      done()
     })
    })
  })

//8. (GET) Een specifieke reservering voor dit apartement
describe('Reservations API GET', () => {
  it('should return an array of Reservations', done => {
    chai
      .request(server)
      .get(endpointreservaton + '/' + 1857)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      const apartment = result[0]

      apartment.should.have.property('ReservationId')
      apartment.should.have.property('ApartmentId')
      apartment.should.have.property('StartDate')
      apartment.should.have.property('EndDate')
      apartment.should.have.property('UserId')
      apartment.should.have.property('Status')
      
      done()
     })
    })
  })

//9. (PUT) Wijzigt de status van deze reservering voor dit apartement. Alleen de eigenaar van het apartement kan een reserveringstatus wijzigen.
describe('Reservations API PUT', () => {
  it('should change the reservation status for an given apartment at given ID', done => {
    chai
    .request(server)
    .put(endpointreservaton + '/' + 1857)
    .set(authorizationHeader, token)
    .send({
      status: 'ACCEPTED' 
    })
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')

      const result = res.body.result
      const apartment = result[0]
      
      done()
     })
    })
  })
//10. (DELETE) Meld de gegeven deelnemer af voor deze activiteit.
describe('Reservation API DELETE', () => {
  //We moeten eerst een reservatie aanmaken voordat we die kunnen verwijderen.
  before(function(done){
    this.enableTimeouts(false) 
    chai
    .request(server)
    .post(endpointreservaton)
    .send({
      status: 'Initial',
      startdate: '2015-2-2',
      enddate: '2019-2-1'
    })
    .set(authorizationHeader, token)
    .end(function(err, res) {
      const result = res.body.result
      const reservation = result[0];
      ReservationToDeleteId = reservation.id;

      done()

    })
  })
 
//Hier wordt het Reservation daadwerkelijk verwijderd.
it('should be able to delete Reservation if the user is the owner of the given Reservation', done => {
  chai
  .request(server)
  .delete(endpointapartments + '/' + ReservationToDeleteId)
  .set(authorizationHeader, token)
  .send()
  .end((err, res) => {
    res.should.have.status(200)
    done()
  })
})
})